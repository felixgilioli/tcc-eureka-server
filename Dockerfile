FROM openjdk:11-jdk-slim
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY ./build/libs/* ./eureka-server-0.0.1-SNAPSHOT.jar
EXPOSE 8761
CMD ["java","-jar","eureka-server-0.0.1-SNAPSHOT.jar"]